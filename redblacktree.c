#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
} ArvoreRB;

int eh_no_vermelho(ArvoreRB * no);
int buscar (ArvoreRB *a, int v);
void in_order(ArvoreRB *a);
void print(ArvoreRB * a,int spaces);

ArvoreRB * novo_no(int info, int cor);
ArvoreRB * rotacao_esq(ArvoreRB * no);
ArvoreRB * rotacao_esq(ArvoreRB * no);
ArvoreRB * corrigir_insercao(ArvoreRB * no);
ArvoreRB * inserir(ArvoreRB * no, int info);
ArvoreRB * corrigir_remocao(ArvoreRB * no);
ArvoreRB * remover_menor(ArvoreRB * raiz, ArvoreRB * no);
ArvoreRB * remover(ArvoreRB * no, int info);


//===============================================================//
int main(){
  ArvoreRB * a = NULL;
  char func;
  int info;

do
  {
      printf("'i' to insert 'r' to remove 's' to quit: ");
      scanf("%c",&func);
      func = tolower(func);

    if(func == 'i'){
        printf("\ninsert: ");
        scanf("%d",&info);
        a = inserir(a, info);
        fflush(stdin);
    }

    if(func == 'r'){
        printf("\nremover: ");
        scanf("%d",&info);
        a = remover(a, info);
        fflush(stdin);
    }

    if (func != 'i' && func != 'r' && func != 's')
        printf("\n\n i', 'r' ou 's' meu colega!\n");
  } while(func != 's');
  printf("\n\n//==================================================//\n\n");
  print(a, 1);
  printf("\n\n//==================================================//\n\n");
}

//===============================================================//

int eh_no_vermelho(ArvoreRB * no){
  if(!no) return BLACK;
  return(no->cor == RED);
}
//---------------------------------------------------------------//
int buscar (ArvoreRB *a, int v) {
  if (a == NULL) { return 0; } /*Nao achou*/
  else if (v < a->info) {
    return buscar (a->esq, v);
  }
  else if (v > a->info) {
    return buscar (a->dir, v);
  }
  else { return 1; } /*Achou*/
}
//---------------------------------------------------------------//
void in_order(ArvoreRB *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}
//---------------------------------------------------------------//
void print(ArvoreRB * a,int spaces){
  int i;
  for(i=0;i<spaces;i++)
    printf(" ");
  if(!a){
    printf("//\n");
    return;
  }

  printf("%d\n", a->info);
  print(a->esq,spaces+2);
  print(a->dir,spaces+2);
}
//===============================================================//
ArvoreRB * novo_no(int info, int cor){
    ArvoreRB * novo = (ArvoreRB *) malloc(sizeof(ArvoreRB));
    novo->info = info;
    novo->cor = cor;
    novo->esq = novo->dir = NULL;
    return novo;
}

//===============================================================//
ArvoreRB * rotacao_esq(ArvoreRB * no){
    ArvoreRB * aux = no->dir;
    no->dir = aux->esq;
    aux->esq = no;
    return aux;
}

//===============================================================//
ArvoreRB * rotacao_dir(ArvoreRB * no){
    ArvoreRB * aux = no->esq;
    no->esq = aux->dir;
    aux->dir = no;
    return aux;
}

//===============================================================//
ArvoreRB * corrigir_insercao(ArvoreRB * no){
    if(eh_no_vermelho(no->dir) && !eh_no_vermelho(no->esq))
        no = rotacao_esq(no);
    if(eh_no_vermelho(no->esq) && eh_no_vermelho(no->esq->esq))
        no = rotacao_dir(no);
    if(eh_no_vermelho(no->esq) && eh_no_vermelho(no->dir))
        no->cor = RED, no->esq->cor = no->dir->cor = BLACK;
    return no;
}

//===============================================================//
ArvoreRB * inserir(ArvoreRB * no, int info){
    if(!no)
        return novo_no(info, RED);
    if(info < no->info)
        no->esq = inserir(no->esq,info);
    else if(info > no->info)
        no->dir = inserir(no->dir,info);
    return corrigir_insercao(no);
}

//===============================================================//
ArvoreRB * corrigir_remocao(ArvoreRB * no){
    if(eh_no_vermelho(no->esq))
        no = rotacao_dir(no);
    if(eh_no_vermelho(no->dir) && !eh_no_vermelho(no->dir->esq))
        no = rotacao_esq(no);
    if(eh_no_vermelho(no->esq) && eh_no_vermelho(no->dir))
        no->cor = RED, no->esq->cor = no->dir->cor = BLACK;
    return no;
}
//===============================================================//
ArvoreRB * remover_menor(ArvoreRB * raiz, ArvoreRB * no){
    if(!no->esq){
        if(no->dir)
            no->dir->cor = BLACK;
        free(no);
        return no->dir;
    }
    if(!eh_no_vermelho(no->esq) && !eh_no_vermelho(no->esq->esq))
        no = corrigir_remocao(no);
    no->esq = remover_menor(raiz, no->esq);
    return corrigir_remocao(no);
}

//===============================================================//
ArvoreRB * remover(ArvoreRB * no, int info){
    if(!no)
        return NULL;
    if(info < no->info){
        if(!eh_no_vermelho(no->esq) && !eh_no_vermelho(no->esq->esq))
            no = corrigir_remocao(no);
            no->esq = remover(no->esq, info);
    }
    else{
        if(eh_no_vermelho(no->esq))
            no = rotacao_dir(no);
        if(info == no->info && !no->dir){
            free(no);
            return NULL;
        }
        if(!eh_no_vermelho(no->dir) && !eh_no_vermelho(no->dir->esq))
            no = corrigir_remocao(no);
        if(info == no->info){
            ArvoreRB * aux = remover_menor(no, no->dir);
            aux->esq = no->esq;
            aux->cor = no->cor;
            free(no);
            return corrigir_remocao(aux);
        }
        no->dir = remover(no->dir, info);
    }
    return corrigir_remocao(no);
}
